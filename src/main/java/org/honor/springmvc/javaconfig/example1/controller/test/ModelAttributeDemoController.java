package org.honor.springmvc.javaconfig.example1.controller.test;

import org.honor.springmvc.javaconfig.example1.domain.Address;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ModelAttributeDemoController {
	private static Logger LOGGER = LoggerFactory.getLogger(ModelAttributeDemoController.class);
	
	@RequestMapping(value="/home")
	public String home() {
		LOGGER.info("inside home: " + System.currentTimeMillis());
		return "modelAttributeHome";
	}
	
	/*
	 * version 2 of home method
	 * using default name "command" for the form backing (command) object
	 * if we use this default naming, we don't have to specify modelAttribute value 
	 * inside the form:form.
	 * */
	@RequestMapping(value="/home2")
	public ModelAndView home2() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("modelAttributeHome");
		mav.addObject("command", new Address());
		return mav;
	}
	
	/*
	 * version 3 of home method. setting the view name inside ModelAndView constructor
	 * */
	@RequestMapping(value="/home3")
	public ModelAndView home3() {
		ModelAndView mav = new ModelAndView("modelAttributeHome");
		mav.addObject("anAddress", new Address());
		return mav;
	}
	
	/*
	 * version 4 of home method. 
	 * */
	@RequestMapping(value="/home4")
	public ModelAndView home4() {
		return new ModelAndView("modelAttributeHome","anAddress", new Address("new york","80220"));
	}
	
	/*
	 * version 5 of home method. 
	 * */
	@RequestMapping(value="/home5")
	public String home5(Model model) {
		model.addAttribute("anAddress",new Address("burdur","01500"));
		return "modelAttributeHome";
	}
	
	/*
	 * Demonstrating the usage of @ModelAttribute on methods
	 * to add multiple attributes
	 * Attributes added below will be available to all handler methods in this controller with @RequestMappping annotation
	 * @ModelAttribute methods are invoked before @RequestMapping methods.
	 * */
	@ModelAttribute
	public void modelAttributeTest1(Model model) {
		LOGGER.info("inside modelAttributeTest1: " + System.currentTimeMillis());
		model.addAttribute("testData1A", "Welcome to the @ModelAttribute testbed.");
		model.addAttribute("testData1B", "Testing usage of @ModelAttribute on methods to add multiple attributes.");
	}
	
	/*
	 * We don't have to explicitly use model.addAttribute as you can see. 
	 * Under the hood spring handles that for us. Returned object (String) is bound as a
	 * model attribute with name "testdata2".
	 * */
	@ModelAttribute(name="testdata2")
	public String modelAttributeTest2() {
		LOGGER.info("inside modelAttributeTest2: " + System.currentTimeMillis());
		return "conducting a series of tests...";
	}
	
	/*
	 * usage of "value" attribute of @ModelAttribute
	 * */
	@ModelAttribute(value="testdata3")
	public Address modelAttributeTest3() {
		LOGGER.info("inside modelAttributeTest3 " + System.currentTimeMillis());
		return new Address("ankara", "0600");
	}
	
	/*
	 * Not specifying ModelAttribute name (or value) explicitly
	 * Spring uses the "type" name. In this case "address" is used as the name of this model attribute.
	 * */
	@ModelAttribute
	public Address modelAttributeTest4() {
		LOGGER.info("inside modelAttributeTest4 " + System.currentTimeMillis());
		return new Address("istanbul","34000");
	}
	
	/*
	 * Testing the @modelAttribute with method arguments 
	 * (with 'value' attribute and default binding) 
	 * @ModelAttribute here binds the posted form data to "Address" bean.
	 * This binding is handled by Spring. 
	 * */
	@RequestMapping(value="/test5", method=RequestMethod.POST)
	public String modelAttributeTest5(@ModelAttribute(value="anAddress") Address anAddress, ModelMap model) {
		LOGGER.info("inside modelAttributeTest5 " + System.currentTimeMillis());
		model.addAttribute("testdata5", anAddress.getCity());
		model.addAttribute("testdata5B", anAddress.getZipCode());
		return "modelAttributeTest";
	}
	
	/*
	 * Test6 : Testing @ModelAttribute and @RequestMapping with no explicit logical view name.
	 * In this case spring resolves the url using the RequestMapping value and modelAttributeTest.jsp is rendered to the
	 * client. This is done automatically using "DefaultRequestToViewNameTranslator" class which is an implementation 
	 * of "RequestToViewNameTranslator" interface.
	 * */
	@ModelAttribute(value="testdata6")
	@RequestMapping(value="/modelAttributeTest")
	public Address modelAttributeTest6() {
		LOGGER.info("inside modelAttributeTest6 " + System.currentTimeMillis());
		return new Address("Antalya", "07000");
	}
}
