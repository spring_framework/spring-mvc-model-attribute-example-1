<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>@ModelAttribute Test Home</title>
</head>
<body>
<h2>@ModelAttribute Test Home</h2>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<div style="text-align:center;">
<a href="${contextPath}/requestMappingParamExample/home">test @RequestMapping</a><br>
<a href="${contextPath}/home2">go to @ModelAttribute test home2</a><br>
<a href="${contextPath}/home3">go to @ModelAttribute test home3</a><br>
<a href="${contextPath}/home4">go to @ModelAttribute test home4</a><br>
<a href="${contextPath}/home5">go to @ModelAttribute test home5</a><br>
</div>
</body>
</html>