<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>@RequestMapping @RequestParam test page</title>
<style>
h2 {
	color:red;
	text-align:center;
}
</style>
</head>
<body>
	<div>
	<h2>>@RequestMapping @RequestParam test page</h2>
	<hr>
	<form action="test1">
		<h3>test1: @RequestParam without explicit attributes</h3>
		<label id="organization-name">Organization Name</label>
		<input type="text" name="orgname" size="40"/>
		<input type="submit" value="submit"/>
	</form>
	<br><br>
		<form action="test2">
		<h3>test2: @RequestMapping method attribute</h3>
		<label id="organization-name">Organization Name</label>
		<input type="text" name="orgname" size="40"/>
		<input type="submit" value="submit"/>
	</form>
		<br><br>
		<form action="test3">
		<h3>test3: @RequestMapping fallback feature</h3>
		<input type="submit" value="submit"/>
		</form>
			<br><br>
		<form action="test4">
		<h3>test4: @RequestParam defaultValue attribute</h3>
		<label id="organization-name">Organization Name</label>
		<input type="text" name="orgname" size="40"/>
		<input type="submit" value="submit"/>
	</form>
				<br><br>
		<form action="test5">
		<h3>test5: @RequestParam without 'name' or 'value'</h3>
		<label id="organization-name">Organization Name</label>
		<input type="text" name="orgname" size="40"/>
		<input type="submit" value="submit"/>
	</form>
	</div>
</body>
</html>