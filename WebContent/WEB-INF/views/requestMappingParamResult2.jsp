<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Request mapping and param test results 2</title>
</head>
<body>
	<h1>${testmethod}</h1>
<c:forEach var="parameter" items="${param}">
<c:out value="${parameter.key}"/> = <c:out value="${parameter.value}"/>
</c:forEach>
</body>
</html>